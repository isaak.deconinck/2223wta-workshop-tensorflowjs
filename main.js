let video;
let poses = [];
let nose;
let currentPose;
let skeleton;

function setup() {
  // Create canvas object

  // Link video (P5 CreateCapture function)

  // Initialize poseNet instance (load TensorFlow.js model)
}

function modelReady() {}

function poseResults(results) {}

function draw() {
  // drawNoseKeyPoint();
  // drawPoseKeypoints();
  // drawPoseSkeleton();
}

// A function to draw an ellipse over the visible nose keypoint
function drawNoseKeyPoint() {
}

// A function to draw ellipses over the visible pose keypoints
function drawPoseKeypoints() {
}

// A function to draw the pose skeleton
function drawPoseSkeleton() {
}
